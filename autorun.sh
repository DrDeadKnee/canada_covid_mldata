#!/bin/bash

# set -o nounset
# set -o errexit
# set -eu

DATE=$(date)
DIR=$(pwd)
source "$DIR/.covid-env/bin/activate"
echo $(pip freeze)

python3 src/pulldata.py -o
python3 src/prepdata.py
git add -f data/prepared/augmented_cases.csv
git commit -m "Updated data to $DATE"
git push
