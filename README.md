# Canada Covid Data

This repository contains data pertaining to covid cases in canada, and the
code used to assemble that data. The final data itself should contain only numeric
values. The purpose of doing this is to make easily available a reasonably rich 
data source for "practice" ML projects, or for anyone interested in exploring
some health data.

All data is sourced from the [Canadian COVID-19 Open Data project](https://resources-covid19canada.hub.arcgis.com/)

the final data incorporates four data sources from the data hub:

- [case data](https://resources-covid19canada.hub.arcgis.com/datasets/compiled-covid-19-case-details-canada)
- [smoking data](https://resources-covid19canada.hub.arcgis.com/datasets/exchange::incidence-of-smoking-by-health-region)
- [health unit demographics](https://resources-covid19canada.hub.arcgis.com/datasets/exchange::contextual-measures-by-health-unit?geometry=-15.950%2C38.665%2C-177.669%2C83.576)
- [medical professional data](https://resources-covid19canada.hub.arcgis.com/datasets/exchange::doctors-by-health-region)


## Installation / setup with virtual environment
```bash
git clone https://DrDeadKnee@bitbucket.org/DrDeadKnee/canada_covid_mld
pip install virtualenv
python3 -m venv .covid-env
source .covid-env/bin/activate
pip install requirements.txt
```

## Usage
```bash
# to overwrite existing pulled data
./run.sh -o

# to run without downloading new data
./run.sh
```
