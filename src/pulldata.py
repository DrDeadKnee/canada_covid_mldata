#!/usr/bin/env python3

import wget
import os
import sys

print("Gathering raw data...")

# Set defaults
overwrite = False

# Read command line args and parse
args = [i.lower() for i in sys.argv]
if "-o" in args:
    overwrite = True


def get_data(filename, url):
    path = os.path.join('data', 'raw', filename)

    if os.path.exists(path) and overwrite:
        os.remove(path)

    if not os.path.exists(path):
        print("\tdownloading {}".format(filename))
        wget.download(url, path)
    else:
        print("\tskipping {}".format(filename))


# Cases
get_data(
    'cases.csv',
    url="https://opendata.arcgis.com/datasets/4dabb4afab874804ba121536efaaacb4_0.csv"
)

# Smoking
get_data(
    "smoking.csv",
    url="https://opendata.arcgis.com/datasets/ad0d85fee7304378aa301f34f89bad7f_0.csv"
)

# Health Units
get_data(
    "hru.csv",
    url="https://opendata.arcgis.com/datasets/1c67c02c1ae349bba350d05854829caa_0.csv"
)

# Doctors
get_data(
    "doctor.csv",
    url="https://opendata.arcgis.com/datasets/07684576bd554711ac7021cd5f910015_0.csv"
)

print("Finished gathering raw data")
