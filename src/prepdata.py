import pandas as pd
import yaml
import os

print("\nPreparing raw data....")
###########################################
# Read in configs and data
########
config = yaml.safe_load(open("config.yaml", "r"))
cases = pd.read_csv(os.path.join("data", "raw", "cases.csv"))
smoking = pd.read_csv(os.path.join("data", "raw", "smoking.csv"))
doctor = pd.read_csv(os.path.join("data", "raw", "doctor.csv"))
hru = pd.read_csv(os.path.join("data", "raw", "hru.csv"))

###########################################
# Prepare Case Data
########

print("Preparing case data")

# Re-scope rows
print("\trescoping")
cases = cases[~cases["hr_uid"].isna()]
cases = cases[cases["case_status"].isin(["Recovered", "Deceased"])]  # active only
cases = cases[cases["gender"].isin(["Male", "Female"])]  # -100 cases > one-hot encoding
cases = cases[cases["age_group"] != "Not Reported"]

# Make Dates Useful
print("\taltering dates")
cases["date_reported"] = pd.to_datetime(cases["date_reported"])
cases["day_zero"] = cases["date_reported"].min()
cases["days_lapsed"] = (cases["date_reported"] - cases["day_zero"]).apply(lambda x: x.days)

# Make Age Group into Numeric
print("\tage_group to age")
cases["age_group"] = cases["age_group"]\
    .str.replace("<20", "10-20")\
    .str.replace("+", "")\
    .str.replace("80", "80-92")\
    .str.split("-").str[1].astype(int) - 5

# Apply Arbitrary maps for binary values
# ccodes will a dictionary with entries like "case_status": {"Recovered": 0, "Deceased": 1}
ccodes = config["cases"]["encodings"]
for key in ccodes:
    cases[key] = cases[key].map(ccodes[key])

# Take only selected columns
kept_cols = config["cases"]["kept_cols"]
cases = cases[kept_cols]

# Write to prepared
cases.to_csv(os.path.join("data", "prepared", "cases.csv"), index=False)


###########################################
# Prepare Smoking Data
########
print("\nAdding smoking data")

# Slim data and merge
print("\tremoving columns and merging")
smoking.columns = [i.lower() for i in smoking.columns]
rawsmokes = smoking.copy()
smoking = smoking[["hr_uid", "m_percent", "f_percent"]]
smoking = cases.merge(smoking, on="hr_uid", how="left")
smoking["synth_smoking"] = 0

# Fill null smoking with mean
print("\treplacing null smoking values by mean value for province and gender")
albertmask1 = smoking["province"] == config["cases"]["encodings"]["province"]["Alberta"]
albertmask2 = rawsmokes.province_territory == "Alberta"
snull = smoking.m_percent.isna()

smoking.loc[(albertmask1 & snull), "m_percent"] = rawsmokes[albertmask2]["m_percent"].mean()
smoking.loc[(albertmask1 & snull), "f_percent"] = rawsmokes[albertmask2]["f_percent"].mean()
smoking.loc[(~albertmask1 & snull), "m_percent"] = rawsmokes[~albertmask2]["m_percent"].mean()
smoking.loc[(~albertmask1 & snull), "f_percent"] = rawsmokes[~albertmask2]["f_percent"].mean()
smoking.loc[snull, "synth_smoking"] = 1

# Create p smoking
print("\tcreating single psmokes column")
smoking["psmokes"] = 0
malemask = smoking["gender"] == config["cases"]["encodings"]["gender"]["Male"]
smoking.loc[malemask, "psmokes"] = smoking["m_percent"]
smoking.loc[~malemask, "psmokes"] = smoking["f_percent"]
del smoking["m_percent"]
del smoking["f_percent"]


###########################################
# Prepare Doctor Data
########
print("\nAdding doctor data")

# Slim data and merge
print("\tremoving columns and merging")
doctor.columns = [i.lower() for i in doctor.columns]
rawdoc = doctor.copy()

keptcols = ["avgage18dr", "percandr", "perfemaledr", "spper100k"]
doccols = ["mean_doc_age", "perc_doc_cndn", "perc_doc_fmale", "spec_per_100k"]
rawdoc = rawdoc.rename({keptcols[i]: doccols[i] for i in range(len(doccols))}, axis=1)

doctor = doctor[["hr_uid"] + keptcols]  # slim
doctor.columns = ["hr_uid"] + doccols  # rename
doctor = smoking.merge(doctor, on="hr_uid", how="left")

# Fill null doctor with mean
print("\treplacing null doctor values by mean value for province")
snull = doctor["mean_doc_age"].isna()

for i in doccols:
    doctor.loc[(snull & albertmask1), i] = rawdoc[rawdoc.prov == "Alberta"][i].mean()
    doctor.loc[(snull & ~albertmask1), i] = rawdoc[rawdoc.prov == "Ontario"][i].mean()

doctor["synth_doc_data"] = 0
doctor.loc[snull, "synth_doc_data"] = 1


###########################################
# Prepare Health Unit Data
########
print("\nAdding Health Unit Data")

# Slim data and merge
print("\tremoving columns and merging")
hru.columns = [i.lower() for i in hru.columns]
rawhru = hru.copy()

keptcols = [
    "drper100k",
    "perrural",
    "perimmigrant",
    "peraboriginal",
    "perchildlowincome",
    "perfoodinsecurity",
    "perrdiabetes",
    "percopd",
    "perhbp",
    "permood"
]
hrucols = [
    "docs_per_100k",
    "p_rural",
    "p_immigrant",
    "p_aboriginal",
    "p_lowinc_haskid",
    "p_food_insecure",
    "p_diabetes",
    "p_copd",
    "p_high_bp",
    "p_mood_disord"
]
rawhru = rawhru.rename({keptcols[i]: hrucols[i] for i in range(len(hrucols))}, axis=1)

hru = hru[["hr_uid"] + keptcols]  # slim
hru.columns = ["hr_uid"] + hrucols  # rename
hru = doctor.merge(hru, on="hr_uid", how="left")

# Fill null food with mean
print("\treplacing food insecurity by mean value for province")
pf = "p_food_insecure"
snull = hru[pf].isna()

hru.loc[(snull & albertmask1), pf] = rawhru[rawhru.province == "AB"][pf].mean()
hru.loc[(snull & ~albertmask1), pf] = rawhru[rawhru.province == "ON"][pf].mean()

hru["synth_food"] = 0
hru.loc[snull, "synth_food"] = 1


###########################################
# Write Final Output
########
hru.to_csv(os.path.join("data", "prepared", "augmented_cases.csv"), index=False)
